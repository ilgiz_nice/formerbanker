import path from 'path'

import express from 'express' //  Express framework
import mongoose from 'mongoose' //  ODM for MongoDB
import cookieParser from 'cookie-parser'  //  Parse cookies from req
import bodyParser from 'body-parser'  //  Parse POST params from req
import socketIO from 'socket.io'  //  Socket io support

import config from './configs/config' //  Config file for app
import routes from './configs/routes' //  Routes for API
import auth from './middleware/auth'  //  Auth page for creating session
import './configs/intervals'

const app = express()

mongoose.connect(config.mongo.url)
mongoose.Promise = Promise

app.use(cookieParser())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use('/static', express.static(path.join(__dirname, './../static')))
app.use('/api', routes)
app.use('/', auth)

const server = app.listen(config.port, () => {
  console.log(`Server started at localhost:${config.port}`)
})

const io = socketIO.listen(server)

io.on('connection', (socket) => {
  socket.on('message', (message) => {
    socket.broadcast.emit('message', message)
  })
})
