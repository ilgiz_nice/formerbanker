import { timer, timeUpdate } from '../controllers/timer' //  Timer to add output to owned agents


const timers = (() => {
  setInterval(() => {
    timer()
  }, 1000 * 60)

  setInterval(() => {
    timeUpdate()
  }, 1000 * 60 * 60)
})()

export default timers
