import express from 'express'

import {
  updateField,
  addAgent,
  writeLogs,
  fetchUser,
  spendTime,
  trainAgent,
  motivateAgent,
  updateProfile,
} from './../controllers/api'

const router = express.Router()

router.route('/tutorial/start').post((req, res) => updateField(req, res, 'tutorial', true))
router.route('/tutorial/game').post((req, res) => updateField(req, res, 'isStarted', true))
router.route('/tutorial/profile').post((req, res) => updateField(req, res, 'elements.profile', true))
router.route('/tutorial/phone').post((req, res) => updateField(req, res, 'elements.phone', true))
router.route('/tutorial/end').post((req, res) => updateField(req, res, 'tutorial', false))

router.route('/agents/add').post((req, res) => addAgent(req, res))
router.route('/agents/train').post((req, res) => trainAgent(req, res))
router.route('/agents/motivate').post((req, res) => motivateAgent(req, res))

router.route('/logs/write').post((req, res) => writeLogs(req, res))

router.route('/users/:id').get((req, res) => fetchUser(req, res))

router.route('/time/spend').post((req, res) => spendTime(req, res))

router.route('/profile/update').post((req, res) => updateProfile(req, res))

export default router
