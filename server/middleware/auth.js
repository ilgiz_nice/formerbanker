import express from 'express'
import request from 'superagent'

import { userRegister, userExists } from './../controllers/auth'
import markup from './../markup'

const router = express.Router()

const html = `
  <html>
    <head>
      <meta charset="UTF-8">
      <title>Регистрация</title>
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBdB9mfwLNnkwkOkrQqG-5pB7jT-fZv18o" type="text/javascript"></script>
      <style>
      .authForm {
        z-index: 10;
        position: absolute;
        left: 35%;
        top: 0px;
        width: 30%;
      }
      @media screen and (max-width: 990px) {
        .authForm {
          left: 15%;
          width: 70%;
        }
      }
      input:not([type=radio]) {
          width: 100%;
          margin-top: 20px;
          text-align: center;
          box-shadow: inset 0 0 8px rgba(102,175,233,.6), 0 0 18px rgba(102,175,233,.6);
          font-size: 30px;
          border: none;
          font-family: cursive;
        }
        input[type=submit] {
          //background-color: rgba(102, 175, 233, .8)
        }
        input[type=radio] {
          width: 30px;
          height: 30px;
          vertical-align: middle;
        }
        .radioItem {
          box-shadow: inset 0 0 8px rgba(102,175,233,.6), 0 0 18px rgba(102,175,233,.6);
          font-size: 18px;
          margin-top: 10px;
          background-color: white;
        }
        #mapContainer {
          position: absolute;
          top: 0px;
          left: 0px;
          z-index: -1;
          width: 100%;
          height: 100%;
        }
      </style>
    </head>
    <body>
      <div id="mapContainer"></div>
      <div class="authFog"></div>
      <form class="authForm" method="POST" action="/auth">
        <div>
          <input id="name" name="name" type="text" placeholder="Имя" />
        </div>
        <div>
          <input id="city" name="city" type="text" placeholder="Город" onkeyup="getLocation(this)" />
        </div>
        <div class="radioItem">
          <input id="type" name="employment" type="radio" value="0" checked="true" /> Я не работаю и готов тратить на проект <b>8 часов в день</b>
        </div>
        <div class="radioItem">
          <input id="type" name="employment" type="radio" value="1" /> Моя работа позволяет тратить на проект <b>5 часов в день</b>
        </div>
        <div class="radioItem">
          <input id="type" name="employment" type="radio" value="2" /> Есть время после работы и я могу тратить <b>3 часа в день</b>
        </div>
        <input type="submit" value="Начать" />
      </form>
      <script>
      window.onload = function() {
        var map = new google.maps.Map(document.getElementById('mapContainer'), {
          zoom: 10,
          minZoom: 10,
          maxZoom: 10,
          center: new google.maps.LatLng(55.75, 37.62),
          disableDefaultUI: true,
          draggableCursor: 'default',
        });
        getLocation = function(e){
          window.clearTimeout(e.f);
          window.getLocationValue = e.value
          e.f = setTimeout(function(){
            var request = new XMLHttpRequest();
            var url = 'https://maps.googleapis.com/maps/api/geocode/json?address='+window.getLocationValue+'&key=AIzaSyBdB9mfwLNnkwkOkrQqG-5pB7jT-fZv18o'
            request.open("GET", url);
            request.onreadystatechange = function () {
              if (request.readyState === 4 && request.status === 200) { 
                try{
                  var response = JSON.parse(request.responseText);
                  var location = response.results[0].geometry.location;
                  map.setCenter({lat: location.lat, lng: location.lng});
                } catch (e){
                  console.log('Что-то пошло не так...');
                }
              }
            }
            request.send(null);
          }, 1500);
        }
      };
      </script>
    </body>
  </html>
`

const setCookie = (req, res) => (
  new Promise((resolve, reject) => {
    const url = encodeURI(`https://maps.googleapis.com/maps/api/geocode/json?address=${req.body.city}&key=AIzaSyBdB9mfwLNnkwkOkrQqG-5pB7jT-fZv18o&language=ru`)
    request(url).end((errCity, responseCity) => {
      if (errCity) return reject(errCity)
      const form = {
        name: req.body.name,
        employment: req.body.employment,
      }
      if (responseCity.body.results.length === 0) {
        form.city = 'Москва'
        form.location = {
          lat: 55.755826,
          lng: 37.6173,
        }
      } else {
        form.city = responseCity.body.results[0].address_components[0].long_name
        form.location = responseCity.body.results[0].geometry.location
      }
      userRegister(form).then((responseRegister) => {
        if (res.cookie('sid', responseRegister.sid)) resolve(true)
        else reject('Не удалось установить cookies.')
      }).catch(errRegister => reject(errRegister))
    })
  })
)

const checkCookie = (req, res) => (
  new Promise((resolve, reject) => {
    userExists(req.cookies.sid).then((response) => {
      if (response === true) {
        resolve(true)
      } else {
        res.redirect('/auth')
      }
    }).catch(err => reject(err))
  })
)

const middleware = (req, res) => {
  if (!req.cookies.sid) {
    setCookie(req, res).then(() => res.redirect('/')).catch(err => res.send(err))
  } else {
    checkCookie(req, res).then(() => res.redirect('/')).catch(err => res.send(err))
  }
}

router.route('/').get((req, res) => {
  checkCookie(req, res).then(() => markup(req, res)).catch(err => res.send(err))
})

router.route('/auth').get((req, res) => res.send(html))
router.route('/auth').post((req, res) => middleware(req, res))

export default router
