import User from '../schemas/users'

const update = (sid, name) => (
  new Promise((resolve, reject) => {
    User.findOne({ sid }, (err, user) => {
      if (err) return reject(err)
      user.name = name
      user.save((_err, _user) => {
        if (_err) return reject(err)
        resolve({ user: _user })
      })
    })
  })
)

export default {
  update,
}
