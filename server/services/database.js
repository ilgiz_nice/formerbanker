import async from 'async'

import User from './../schemas/users'
import Agent from './../schemas/agents'
import Log from './../schemas/logs'
import { findPlace } from './../controllers/api'

const names = ['Агент 1', 'Агент 2', 'Агент 3', 'Агент 4', 'Агент 5', 'Агент 6']

const createGroups = userA => (
  new Promise((resolve, reject) => {
    const array = []
    const functions = []
    const groups = [
      {
        name: 'Родственник',
        timeCost: 1,
      },
      {
        name: 'Сосед по дому',
        timeCost: 3,
      },
      {
        name: 'Бывший однокурсник',
        timeCost: 2,
      },
    ]
    groups.forEach(() => functions.push(async.apply(findPlace, userA.location)))
    async.parallel(functions, (err, results) => {
      if (err) reject(err)
      groups.forEach((el, i) => {
        const agents = []
        let count = Math.floor(Math.random() * 5) + 1
        while (count > 0) {
          const age = Math.floor(Math.random() * (61 - 20)) + 20
          let timeCost
          switch (true) {
            case (age >= 20 && age <= 30):
              timeCost = 1
              break
            case (age > 30 && age <= 40):
              timeCost = 2
              break
            case (age > 40 && age <= 50):
              timeCost = 3
              break
            case (age > 50):
              timeCost = 4
              break
            default:
              timeCost = 4
              break
          }
          agents.push({
            name: names[count],
            age,
            timeCost,
          })
          count -= 1
        }
        array.push({
          group: el.name,
          timeCost: el.timeCost,
          company: results[i].company,
          location: { lat: results[i].location.lat, lng: results[i].location.lng },
          agents,
        })
      })
      resolve(array)
    })
  })
)

export const dbUserExists = sid => (
  new Promise((resolve, reject) => {
    const query = User.where({ sid })
    query.findOne((err, _user) => {
      if (err) reject(err)
      resolve(_user)
    })
  })
)

export const dbUserRegister = (sid, { name, employment, city, location: { lat, lng } }) => (
  new Promise((resolve, reject) => {
    const time = () => {
      switch (parseInt(employment, 10)) {
        case 0:
          return 8
        case 1:
          return 5
        case 2:
          return 3
        default:
          return 0
      }
    }
    const user = new User({
      sid,
      name,
      city,
      employment,
      time: time(),
      location: {
        lat,
        lng,
      },
    })
    user.save((errA, userA) => {
      if (errA) reject(errA)
      createGroups(userA).then((agents) => {
        const agent = new Agent({ _owner: userA._id, potentialAgents: agents })
        const log = new Log({ _owner: userA._id, logs: [] })
        agent.save((errB, agentsA) => {
          if (errB) reject(errB)
          log.save((errC, logsA) => {
            if (errC) return reject(errC)
            return resolve({ userA, agentsA, logsA })
          })
        })
      })
    })
  })
)

export const dbGet = sid => (
  new Promise((resolve, reject) => {
    Agent.find().populate('_owner').exec((err, users) => {
      if (err) reject(err)
      else resolve(users.filter(u => u._owner.sid === sid)[0])
    })
  })
)

export const dbUpdate = (sid, key, value) => (
  new Promise((resolve, reject) => {
    const params = {}
    params[key] = value
    User.findOneAndUpdate({ sid }, params, { new: true }, (err, user) => {
      if (err) reject(err)
      else resolve(user)
    })
  })
)

export const dbAddAgent = (sid, group, agent) => (
  new Promise((resolve, reject) => {
    Agent.find().populate('_owner').exec((errA, users) => {
      if (errA) return reject(errA)
      const userA = users.filter(u => u._owner.sid === sid)[0]  //  Current user
      if (!userA) return reject('User was not found.')
      if (userA._owner.time < agent.timeCost) return reject('Не хватает времени')
      const groupA = userA.potentialAgents.filter(a => a.group === group)[0]
      if (!groupA) return reject('Group was not found.')
      Agent.findOne({ _id: userA._id }, (errB, result) => {
        if (errB) return reject(errB)
        for (let i = 0; i < result.potentialAgents.length; i += 1) {
          const agentsA = result.potentialAgents[i]
          if (agentsA.group === group) {
            const index = agentsA.agents.indexOf(agent)
            agentsA.agents.splice(index, 1)
            break
          }
        }
        const item = result.ownedAgents.filter(owned => owned.group === group)
        if (item.length > 0) {
          for (let i = 0; i < result.ownedAgents.length; i += 1) {
            if (result.ownedAgents[i]._id.toString() === item[0]._id.toString()) {
              result.ownedAgents[i].agents.push({
                name: agent.name,
                age: agent.age,
              })
            }
          }
        } else {
          result.ownedAgents.push({
            group,
            company: groupA.company,
            location: {
              lat: groupA.location.lat,
              lng: groupA.location.lng,
            },
            agents: [
              {
                name: agent.name,
                age: agent.age,
              },
            ],
          })
        }
        result.save((errC, agentsB) => {
          if (errC) return reject(errC)
          User.findOne({ _id: userA._owner._id }, (errD, userB) => {
            if (errD) return reject(errD)
            userB.time -= agent.timeCost
            userB.save((errE, userC) => {
              if (errE) return reject(errE)
              resolve({ agentsB, userC })
            })
          })
        })
      })
    })
  })
)

export const dbTimer = () => (
  new Promise((resolve, reject) => {
    Agent.find({}, (errA, agents) => {
      if (errA) return reject(errA)
      if (!agents) return reject('Agents were not found.')
      for (let i = 0; i < agents.length; i += 1) {
        for (let j = 0; j < agents[i].ownedAgents.length; j += 1) {
          for (let k = 0; k < agents[i].ownedAgents[j].agents.length; k += 1) {
            let output = agents[i].ownedAgents[j].agents[k].output
            if (agents[i].ownedAgents[j].agents[k].bonusActive) {
              output *= 1.6
            }
            agents[i].ownedAgents[j].agents[k].total += output
          }
        }
      }
      agents.forEach((a) => {
        a.save((errB, agent) => {
          if (errB) return reject(errB)
        })
      })
    })
  })
)

export const dbRating = () => (
  new Promise((resolve, reject) => {
    Agent.find({ ownedAgents: { $gt: [] } }).populate('_owner').exec((err, users) => {
      if (err) return reject(err)
      const rating = users.map((user) => {
        const total = user.ownedAgents.reduce((group, u) => (
          group + u.agents.reduce((sum, agent) => (sum + agent.total), 0)
        ), 0)
        const average = user.ownedAgents.reduce((group, u) => (
          group + (u.agents.length * 100)
        ), 0)
        return {
          _id: user._owner._id,
          total,
          average,
        }
      })
      return resolve(rating)
    })
  })
)

export const dbWriteLogs = (sid, event) => (
  new Promise((resolve, reject) => {
    Log.find().populate('_owner').exec((err, logs) => {
      if (err) return reject(err)
      let index
      for (let i = 0; i < logs.length; i += 1) {
        const item = logs[i]
        if (item._owner.sid === sid) {
          item.logs.push({
            event,
          })
          index = i
          break
        }
      }
      logs[index].save((errSave, saved) => {
        if (errSave) return reject(errSave)
        return resolve(saved)
      })
    })
  })
)

export const dbReadLogs = sid => (
  new Promise((resolve, reject) => {
    Log.find().populate('_owner').exec((err, logs) => {
      if (err) return reject(err)
      for (let i = 0; i < logs.length; i += 1) {
        const item = logs[i]
        if (item._owner.sid === sid) {
          return resolve(item)
        }
      }
    })
  })
)

export const dbFetchUser = id => (
  new Promise((resolve, reject) => {
    Agent.find().populate('_owner').exec((errA, agents) => {
      if (errA) return reject(errA)
      Log.find().populate('_owner').exec((errB, logs) => {
        if (errB) return reject(errB)
        const agent = agents.filter(a => a._owner._id.toString() === id)[0]
        const log = logs.filter(l => l._owner._id.toString() === id)[0]
        resolve({ owner: agent._owner, agent, log })
      })
    })
  })
)

export const dbTimeSpend = (sid, amount) => (
  new Promise((resolve, reject) => {
    User.findOne({ sid }, (err, userA) => {
      if (err) return reject(err)
      if (!userA) return reject('Пользователь не найден')
      if (userA.time < amount) return reject('Не хватает времени')
      userA.time -= amount
      userA.save((errB, userB) => {
        if (errB) return reject(errB)
        return resolve(userB)
      })
    })
  })
)

// export const dbTimeAdd = (sid, amount) => (
//   new Promise((resolve, reject) => {
//     User.findOne({ sid }, (err, userA) => {
//       if (err) return reject(err)
//       userA.time += amount
//       userA.save((errB, userB) => {
//         if (errB) return reject(errB)
//         return resolve(userB)
//       })
//     })
//   })
// )

export const dbTimeUpdate = () => (
  new Promise((resolve, reject) => {
    User.find({}, (errA, users) => {
      if (errA) return reject(errA)
      for (let i = 0; i < users.length; i += 1) {
        const user = users[i]
        switch (user.employment) {
          case 0:
            user.time = 8
            break
          case 1:
            user.time = 5
            break
          case 2:
            user.time = 3
            break
          default:
            user.time = 0
            break
        }
      }
      users.forEach((userA) => {
        userA.save((errB, userB) => {
          if (errB) return reject(errB)
        })
      })
      return resolve('time update done')
    })
  })
)

export const dbAgentTrain = (sid, group, agent) => (
  new Promise((resolve, reject) => {
    Agent.find().populate('_owner').exec((errA, agentsA) => {
      if (errA) return reject(errA)
      const userA = agentsA.filter(a => a._owner.sid === sid)[0]
      if (!userA) return reject('Пользователя не найдено')
      Agent.findOne({ _id: userA._id }).populate('_owner').exec((errB, agentsB) => {
        if (errB) return reject(errB)
        for (let i = 0; i < agentsB.ownedAgents.length; i += 1) {
          if (agentsB.ownedAgents[i].group === group) {
            for (let j = 0; j < agentsB.ownedAgents[i].agents.length; j += 1) {
              if (agentsB.ownedAgents[i].agents[j].name === agent.name) {
                const output = agentsB.ownedAgents[i].agents[j].output * 1.1
                agentsB.ownedAgents[i].agents[j].output = parseInt(output, 10)
              }
            }
          }
        }
        agentsB.save((errC, agentsC) => {
          if (errC) return reject(errC)
          User.findOne({ sid: sid }, (errD, userB) => {
            if (errD) return reject(errD)
            userB.time -= 6
            userB.save((errE, userC) => {
              if (errE) return reject(errE)
              resolve({ agents: agentsC, user: userC })
            })
          })
        })
      })
    })
  })
)

Date.prototype.addHours = function (h) {
  this.setTime(this.getTime() + (h * 60 * 60 * 1000))
  return this
}

export const dbAgentMotivate = (sid, group, agent) => (
  new Promise((resolve, reject) => {
    Agent.find().populate('_owner').exec((errA, agentsA) => {
      if (errA) return reject(errA)
      const userA = agentsA.filter(a => a._owner.sid === sid)[0]
      if (!userA) return reject('Пользователя не найдено')
      Agent.findOne({ _id: userA._id }).populate('_owner').exec((errB, agentsB) => {
        if (errB) return reject(errB)
        for (let i = 0; i < agentsB.ownedAgents.length; i += 1) {
          if (agentsB.ownedAgents[i].group === group) {
            for (let j = 0; j < agentsB.ownedAgents[i].agents.length; j += 1) {
              if (agentsB.ownedAgents[i].agents[j].name === agent.name) {
                agentsB.ownedAgents[i].agents[j].bonusActive = true
                agentsB.ownedAgents[i].agents[j].bonusStartDate = new Date().addHours(6)
              }
            }
          }
        }
        agentsB.save((errC, agentsC) => {
          if (errC) return reject(errC)
          User.findOne({ sid: sid }, (errD, userB) => {
            if (errD) return reject(errD)
            userB.time -= 3
            userB.save((errE, userC) => {
              if (errE) return reject(errE)
              resolve({ agents: agentsC, user: userC })
            })
          })
        })
      })
    })
  })
)
