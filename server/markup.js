import { createStore } from 'redux'

import reducers from './../client/redux/reducers'
import { dbGet, dbRating, dbReadLogs } from './services/database'
import hintsDB from './../client/configs/hints'

const renderFullPage = (preloadedState, owner) => (
  `
    <html>
      <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>Former banker game</title>
      </head>
      <body>
        <div id="app"></div>
        <script>
          window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState)}
          window.__isMobile = (typeof window.orientation !== 'undefined')
          window.__USER__ = ${JSON.stringify({ id: owner._id, name: owner.name, location: owner.location })}
        </script>
        <script src="/static/bundle.js"></script>
      </body>
    </html>
  `
)

const handleRender = (req, res) => {
  const store = createStore(reducers)
  const preloadedState = store.getState()

  if (!req.cookies.sid) return res.redirect('/auth')

  Promise.all([
    dbGet(req.cookies.sid),
    dbRating(),
    dbReadLogs(req.cookies.sid),
  ]).then((response) => {
    const data = response[0]
    const owner = data._owner
    const rating = response[1]
    const logs = response[2]
    preloadedState.tutorial.isStarted = owner.isStarted
    preloadedState.tutorial.tutorial = owner.tutorial
    preloadedState.tutorial.profile = owner.elements.profile
    preloadedState.tutorial.phone = owner.elements.phone
    preloadedState.async.time = owner.time
    preloadedState.async.agents.ownedAgents = data.ownedAgents
    preloadedState.async.agents.potentialAgents = data.potentialAgents
    let hints = []
    if (!owner.isStarted) {
      hints = hintsDB.slice(0, 4)
    } else if (owner.tutorial && owner.elements.profile && !owner.elements.phone) {
      hints = hintsDB.slice(4, 6)
    } else if (owner.tutorial && owner.elements.phone) {
      hints = hintsDB.slice(4, 7)
    } else {
      hints = hintsDB.slice(7)
    }
    preloadedState.elements.hints = hints
    preloadedState.async.rating = rating
    preloadedState.async.logs = logs.logs
    preloadedState.elements.map.lat = owner.location.lat
    preloadedState.elements.map.lng = owner.location.lng
    preloadedState.chats.cities = [{
      name: owner.city,
      messages: [],
    }]
    preloadedState.async.profile.name = owner.name
    preloadedState.async.profile.city = owner.city
    res.send(renderFullPage(preloadedState, owner))
  }).catch((err) => {
    console.log(err)
    res.send('Произошла ошибка в загрузке первоначальных данных.')
  })
}

export default handleRender
