import shortid from 'shortid'

import { dbUserExists, dbUserRegister } from './../services/database'

export const userExists = sid => (
  new Promise((resolve, reject) => {
    dbUserExists(sid).then((res) => {
      if (res) resolve(true)
      else resolve(false)
    }).catch(err => reject(err))
  })
)

export const userRegister = (form) => (
  new Promise((resolve, reject) => {
    const sid = shortid.generate()
    dbUserRegister(sid, form).then(({ userA }) => {
      if (userA) resolve(userA)
      else resolve(false)
    }).catch(err => reject(err))
  })
)
