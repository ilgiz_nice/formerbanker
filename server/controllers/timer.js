import { dbTimer, dbTimeUpdate } from './../services/database'

export const timer = () => {
  dbTimer().then((response) => {
    console.log(response)
  }).catch(err => console.log(err))
}

export const timeUpdate = () => {
  dbTimeUpdate().then((response) => {
    console.log(response)
  }).catch(err => console.log(err))
}
