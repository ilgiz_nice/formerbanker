import request from 'superagent'

import {
  dbUpdate,
  dbAddAgent,
  dbWriteLogs,
  dbFetchUser,
  dbTimeSpend,
  dbAgentTrain,
  dbAgentMotivate,
} from './../services/database'

import Profile from '../services/profile'

const nearbysearch = ({ lat, lng }) => {
  const searchTypes = ['bakery', 'bank', 'bar', 'beauty_salon', 'book_store', 'cafe', 'car_dealer', 'car_repair', 'car_wash', 'cemetery', 'clothing_store', 'convenience_store',
    'courthouse', 'department_store', 'electronics_store', 'embassy', 'finance', 'food', 'furniture_store', 'gas_station', 'general_contractor',
    'grocery_or_supermarket', 'gym', 'hair_care', 'hardware_store', 'home_goods_store', 'hospital', 'insurance_agency', 'jewelry_store', 'lawyer', 'library',
    'liquor_store', 'local_government_office', 'meal_delivery', 'meal_takeaway', 'movie_theater', 'museum', 'night_club', 'parking', 'pet_store',
    'pharmacy', 'place_of_worship', 'police', 'post_office', 'real_estate_agency', 'restaurant', 'school', 'shoe_store', 'shopping_mall', 'store', 'travel_agency', 'university']
  const host = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?'
  const locationLat = lat + (0.15 * (Math.random() > 0.5 ? +Math.random() : -Math.random()))
  const locationLng = lng + (0.15 * (Math.random() > 0.5 ? +Math.random() : -Math.random()))
  const location = `location=${locationLat},${locationLng}`
  const radius = 'radius=5000'
  const types = searchTypes.join('|')
  const key = 'key=AIzaSyBdB9mfwLNnkwkOkrQqG-5pB7jT-fZv18o'
  return `${host}${location}&${radius}&types=${types}&${key}`
}

export const findPlace = (location, callback) => {
  const url = nearbysearch({ lat: location.lat, lng: location.lng })
  console.log(url)
  request(url).end((err, response) => {
    if (err) {
      callback(err)
    } else {
      const places = JSON.parse(response.text)
      const place = places.results[Math.floor(Math.random() * places.results.length)]
      callback(null, { location: place.geometry.location, company: place.name })
    }
  })
}

export const updateField = (req, res, _key, value) => {
  dbUpdate(req.cookies.sid, _key, value).then((response) => {
    if (response) res.send(true)
    else res.sendStatus(500)
  }).catch((err) => {
    console.log(err)
    res.sendStatus(500)
  })
}

export const addAgent = (req, res) => {
  dbAddAgent(req.cookies.sid, req.body.payload.group, req.body.payload.agent).then((response) => {
    if (response) res.send(response)
    else res.sendStatus(500)
  }).catch((err) => {
    console.log(err)
    res.sendStatus(500)
  })
}

export const writeLogs = (req, res) => {
  dbWriteLogs(req.cookies.sid, req.body.payload.event).then((response) => {
    if (response) res.send(response)
    else res.sendStatus(500)
  }).catch((err) => {
    console.log(err)
    res.sendStatus(500)
  })
}

export const fetchUser = (req, res) => {
  dbFetchUser(req.params.id).then((response) => {
    if (response) res.send(response)
    else res.sendStatus(500)
  }).catch((err) => {
    console.log(err)
    res.sendStatus(500)
  })
}

export const spendTime = (req, res) => {
  dbTimeSpend(req.cookies.sid, req.body.payload.amount).then((response) => {
    if (response) res.send(response)
    else res.sendStatus(500)
  }).catch((err) => {
    console.log(err)
    res.sendStatus(500)
  })
}

export const trainAgent = (req, res) => {
  dbAgentTrain(req.cookies.sid, req.body.payload.group, req.body.payload.agent).then((response) => {
    if (response) res.send(response)
    else res.sendStatus(500)
  }).catch((err) => {
    console.log(err)
    res.sendStatus(500)
  })
}

export const motivateAgent = (req, res) => {
  dbAgentMotivate(req.cookies.sid, req.body.payload.group, req.body.payload.agent).then((response) => {
    if (response) res.send(response)
    else res.sendStatus(500)
  }).catch((err) => {
    console.log(err)
    res.sendStatus(500)
  })
}

export const updateProfile = (req, res) => {
  console.log(req.body.payload)
  Profile.update(req.cookies.sid, req.body.payload.name).then((response) => {
    if (response) res.send(response)
    else res.sendStatus(500)
  }).catch((err) => {
    console.log(err)
    res.sendStatus(500)
  })
}
