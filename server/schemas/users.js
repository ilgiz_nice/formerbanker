import mongoose from 'mongoose'

const Schema = mongoose.Schema

const User = new Schema({
  sid: String,
  name: String,
  city: String,
  employment: Number,
  time: Number,
  location: {
    lat: Number,
    lng: Number,
  },
  isStarted: { type: Boolean, default: false },
  tutorial: { type: Boolean, default: false },
  elements: {
    profile: { type: Boolean, default: false },
    phone: { type: Boolean, default: false },
  },
  _agents: { type: Schema.Types.ObjectId, ref: 'agents' },
  _logs: { type: Schema.Types.ObjectId, ref: 'logs' },
  _chats: { type: Schema.Types.ObjectId, ref: 'chats' },
})

export default mongoose.model('users', User)
