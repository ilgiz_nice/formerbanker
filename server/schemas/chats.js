import mongoose from 'mongoose'

const Schema = mongoose.Schema

const Chat = new Schema({
  _owner: { type: Schema.Types.ObjectId, ref: 'users' },
  messages: [
    {
      from: Schema.Types.ObjectId,
      to: Schema.Types.ObjectId,
      text: String,
      date: { type: Date, default: new Date() },
    },
  ],
})

export default mongoose.model('chats', Chat)
