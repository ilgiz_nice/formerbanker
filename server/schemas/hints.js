import mongoose from 'mongoose'

const Schema = mongoose.Schema

const Hint = new Schema({
  type: String,
  hints: [
    {
      img: { type: String, default: '/static/alena.jpg' },
      text: String,
    },
  ],
})

export default mongoose.model('hints', Hint)
