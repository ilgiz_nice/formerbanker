import mongoose from 'mongoose'

const Schema = mongoose.Schema

const Log = new Schema({
  _owner: { type: Schema.Types.ObjectId, ref: 'users' },
  logs: [
    {
      date: { type: Date, default: new Date() },
      event: String,
    },
  ],
})

export default mongoose.model('logs', Log)
