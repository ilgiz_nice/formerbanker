import mongoose from 'mongoose'

const Schema = mongoose.Schema

const Agent = new Schema({
  _owner: { type: Schema.Types.ObjectId, ref: 'users' },
  ownedAgents: [
    {
      group: String,
      company: String,
      location: {
        lat: Number,
        lng: Number,
      },
      agents: [
        {
          name: String,
          age: Number,
          total: { type: Number, default: 0 },
          output: { type: Number, default: 100 },
          bonusActive: { type: Boolean, default: false },
          bonusStartDate: { type: Date, default: new Date() },
        },
      ],
    },
  ],
  potentialAgents: [
    {
      group: String,
      timeCost: Number,
      company: String,
      location: {
        lat: Number,
        lng: Number,
      },
      agents: [
        {
          name: String,
          age: Number,
          timeCost: Number,
        },
      ],
    },
  ],
})

export default mongoose.model('agents', Agent)
