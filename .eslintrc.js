module.exports = {
    "extends": "airbnb",
    "installedESLint": true,
    "plugins": [
        "react",
        "jsx-a11y",
        "import",
    ],
    "env": {
      "browser": true,
      "node": true,
    },
    "rules": {
        semi: [2, 'never'],
        noStaticElementInteraction: 0,
        "no-underscore-dangle": 0,
    },
    "parser": "babel-eslint",
};
