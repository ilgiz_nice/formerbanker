import io from 'socket.io-client'

import { aChatAsync } from './redux/actions/async'

export default (state) => {

  const socket = io()

  socket.on('message', (message) => {
    state.dispatch(aChatAsync.receiveMessage(message))
  })

  const send = (message) => {
    socket.emit('message', message)
    state.dispatch(aChatAsync.receiveMessage(message))
  }

  return send
}
