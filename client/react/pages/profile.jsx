import React, { PropTypes } from 'react'

import Profile from './../containers/profile.jsx'

const Container = ({
  route: { dispatch },
  params: { id },
}) => {
  dispatch({
    type: 'FETCH_USER',
    payload: {
      id,
    },
  })
  return (
    <Profile id={id} />
  )
}

Container.propTypes = {
  route: PropTypes.shape(),
  params: PropTypes.shape(),
}

export default Container
