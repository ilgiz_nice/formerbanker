import React, { PropTypes } from 'react'

import Hints from './../containers/hints.jsx'
import Elements from './../containers/elements.jsx'
import Map from './../containers/map.jsx'
import Modals from './../containers/modals.jsx'

const Container = ({ route: { sendMessage } } = { route: { sendMessage: null } }) => (
  <div>
    <Hints />
    <Elements />
    <Map />
    <Modals sendMessage={sendMessage} />
  </div>
)

Container.propTypes = {
  route: PropTypes.shape(),
}

export default Container
