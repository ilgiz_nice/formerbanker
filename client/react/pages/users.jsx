import React, { PropTypes } from 'react'

import Hints from './../containers/hints.jsx'
import Elements from './../containers/elements.jsx'
import Map from './../containers/map.jsx'
import Modals from './../containers/modals.jsx'

const Container = ({
  route: { dispatch },
  params: { id },
}) => {
  dispatch({
    type: 'FETCH_USER',
    payload: {
      id,
    },
  })
  return (
    <div>
      <Hints mode="read" />
      <Elements mode="read" />
      <Map mode="read" />
      <Modals mode="read" />
    </div>
  )
}

Container.propTypes = {
  route: PropTypes.shape(),
  params: PropTypes.shape(),
}

export default Container
