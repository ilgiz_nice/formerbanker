import React, { PropTypes } from 'react'
import GoogleMap from 'google-map-react'

import MarkerOwned from './map/markerOwned.jsx'
import MarkerPotential from './map/markerPotential.jsx'

const Map = ({
  map: { lat, lng, zoom, showMarker },
  ownedAgents,
  openDialog,
  changeMap,
}) => (
  <GoogleMap
    center={[lat, lng]}
    zoom={zoom}
    bootstrapURLKeys={{
      key: 'AIzaSyBdB9mfwLNnkwkOkrQqG-5pB7jT-fZv18o',
      language: 'ru',
    }}
    onChange={map => changeMap(map)}
  >
    {ownedAgents.map((group, i) => (
      <MarkerOwned
        key={i}
        lat={group.location.lat}
        lng={group.location.lng}
        zoom={zoom}
      />
    ))}
    {(showMarker === true) && (
      <MarkerPotential
        key="marker"
        lat={lat}
        lng={lng}
        zoom={zoom}
        openDialog={openDialog}
      />
    )}
  </GoogleMap>
)

Map.propTypes = {
  map: PropTypes.shape(),
  ownedAgents: PropTypes.arrayOf(PropTypes.shape()),
  openDialog: PropTypes.func.isRequired,
  changeMap: PropTypes.func.isRequired,
}

export default Map
