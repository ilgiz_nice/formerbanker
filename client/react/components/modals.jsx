import React, { PropTypes } from 'react'

import Log from './modals/logs.jsx'
import Rating from './modals/rating.jsx'
import Profile from './modals/profile.jsx'
import Phone from './modals/phone.jsx'
import Chat from './modals/chats.jsx'
import Dialog from './modals/dialog.jsx'

require('!style!css!sass!../../../static/styles/modals.scss')

const Modals = ({
  //  MODALS
  log,
  rating,
  profile,
  phone,
  //  LOGS
  logList,
  //  RATING
  ratingList,
  //  PROFILE
  ownedAgents,
  trainAgent,
  motivateAgent,
  //  PHONE
  potentialAgents,
  updateMap,
  //  CHATS
  chat,
  tab,
  chats,
  switchTab,
  sendMessage,
  //  DIALOG
  dialog,
  agents: { group, company, agents },
  addAgent,
}) => (
  <div>
    {dialog &&
      <Dialog group={group} company={company} agents={agents} addAgent={addAgent} />
    }
    {log &&
      <Log logList={logList} />
    }
    {rating &&
      <Rating ratingList={ratingList} />
    }
    {profile &&
      <Profile ownedAgents={ownedAgents} trainAgent={trainAgent} motivateAgent={motivateAgent} />
    }
    {phone &&
      <Phone potentialAgents={potentialAgents} updateMap={updateMap} />
    }
    {chat && <Chat tab={tab} chats={chats} switchTab={switchTab} sendMessage={sendMessage} />}
  </div>
)

Modals.propTypes = {
  //  MODALS
  profile: PropTypes.bool,
  phone: PropTypes.bool,
  rating: PropTypes.bool,
  log: PropTypes.bool,
  //  LOGS
  logList: PropTypes.arrayOf(PropTypes.shape),
  //  RATING
  ratingList: PropTypes.arrayOf(PropTypes.shape),
  //  PROFILE
  ownedAgents: PropTypes.arrayOf(PropTypes.shape),
  trainAgent: PropTypes.func.isRequired,
  motivateAgent: PropTypes.func.isRequired,
  //  PHONE
  potentialAgents: PropTypes.arrayOf(PropTypes.shape),
  updateMap: PropTypes.func.isRequired,
  //  CHATS
  chat: PropTypes.bool,
  tab: PropTypes.shape(),
  chats: PropTypes.shape(),
  switchTab: PropTypes.func.isRequired,
  sendMessage: PropTypes.func.isRequired,
  //  DIALOG
  dialog: PropTypes.bool,
  agents: PropTypes.shape(),
  addAgent: PropTypes.func.isRequired,
}

Modals.defaultProps = {
  ownedAgents: [],
  potentialAgents: [],
  profile: false,
  phone: false,
  rating: false,
  log: false,
  chat: false,
}

export default Modals
