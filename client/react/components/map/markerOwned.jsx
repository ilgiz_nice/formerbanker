import React, { PropTypes, Component } from 'react'

export default class Marker extends Component {
  render() {
    const zoom = this.props.zoom
    const marker = {
      width: (zoom * 3) + 'px',
      height: (zoom * 3) + 'px',
      backgroundColor: 'green',
      borderRadius: '50%',
    }
    return (
      <div
        style={marker}
      />
    )
  }
}

Marker.propTypes = {
  zoom: PropTypes.number,
}
