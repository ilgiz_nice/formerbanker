import React, { PropTypes, Component } from 'react'

export default class Marker extends Component {
  render() {
    const zoom = this.props.zoom
    const marker = {
      width: (zoom * 3) + 'px',
      height: (zoom * 3) + 'px',
      backgroundColor: 'red',
      borderRadius: '50%',
    }
    return (
      <div
        style={marker}
        onClick={() => {
          if (!this.props.dialog) {
            this.props.openDialog()
          }
        }}
      />
    )
  }
}

Marker.propTypes = {
  dialog: PropTypes.bool,
  openDialog: PropTypes.func.isRequired,
}
