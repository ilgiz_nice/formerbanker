import React, { PropTypes } from 'react'

require('!style!css!sass!../../../static/styles/hints.scss')

const Hints = ({ hints, isStarted, read, start }) => {
  if (read) return null
  return (
    <div>
      <div className="dialog">
        {hints.map((hint, i) => (
          <div key={i} className="item">
            <img src={hint.img} className="image" alt="Бизнес-тренер" />
            <div>{hint.text}</div>
          </div>
        )) }
        {!isStarted && (
          <div className="item">
            <img src="/static/images/default_user.jpg" className="image" alt="Начать игру" />
            <button className="button" onClick={() => start()} >Начать игру</button>
          </div>
        ) }
      </div>
    </div>
  )
}

Hints.propTypes = {
  hints: PropTypes.arrayOf(PropTypes.shape),
  isStarted: PropTypes.bool.isRequired,
  read: PropTypes.bool,
  start: PropTypes.func.isRequired,
}

Hints.defaultProps = {
  read: false,
}

export default Hints
