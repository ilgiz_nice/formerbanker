import React, { PropTypes } from 'react'
import { Link } from 'react-router'

require('!style!css!sass!./../../../static/styles/elements.scss')

const Elements = ({
  home,
  profile,
  phone,
  rating,
  log,
  tutorial,
  time,
  chat,
  dialog,
  openProfile,
  openPhone,
  openRating,
  openLog,
  openChat,
  currentTime,
  closeDialog,
}) => (
  <div className="menu">
    {home && (
      <Link to={'/'}>
        <div className="icons home" />
      </Link>
    )}
    {rating && (
      <div
        className="icons rating"
        onClick={() => openRating()}
      />
    )}
    {profile && (
      <div
        className="icons profile"
        onClick={() => openProfile(tutorial)}
      />
    )}
    {phone && (
      <div
        className="icons phone"
        onClick={() => openPhone(tutorial)}
      />
    )}
    {log && (
      <div
        className="icons log"
        onClick={() => openLog()}
      />
    )}
    {dialog && (
      <div
        className="icons back"
        onClick={() => closeDialog()}
      />
    )}
    {time && (
      <div className="icons time">
        <div className="timeLabel">{currentTime}</div>
      </div>
    )}
    {chat && (
      <div
        className="icons chat"
        onClick={() => openChat()}
      />
    )}
    <Link to={'/profile'}>
      <div className="icons profile" />
    </Link>
  </div>
)

Elements.propTypes = {
  home: PropTypes.bool,
  profile: PropTypes.bool,
  phone: PropTypes.bool,
  rating: PropTypes.bool,
  log: PropTypes.bool,
  tutorial: PropTypes.bool,
  time: PropTypes.bool,
  chat: PropTypes.bool,
  dialog: PropTypes.bool,
  openProfile: PropTypes.func.isRequired,
  openPhone: PropTypes.func.isRequired,
  openRating: PropTypes.func.isRequired,
  openLog: PropTypes.func.isRequired,
  openChat: PropTypes.func.isRequired,
  currentTime: PropTypes.number.isRequired,
  closeDialog: PropTypes.func.isRequired,
}

Elements.defaultProps = {
  home: false,
  profile: true,
  phone: true,
  rating: true,
  log: true,
  time: true,
  chat: true,
  dialog: false,
}

export default Elements
