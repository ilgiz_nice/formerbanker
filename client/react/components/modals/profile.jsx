import React, { PropTypes } from 'react'

const Profile = ({ ownedAgents, trainAgent, motivateAgent }) => (
  <div className="modal">
    <h3>Список агентов</h3>
    <div className="body">
      {ownedAgents.length > 0 ?
        <table className="table">
          <tbody>
            <tr>
              <th className="td" />
              <th className="td">Имя</th>
              <th className="td">Возраст</th>
              <th className="td">Доход в сутки</th>
              <th className="td">Общий доход</th>
              <th className="td">Бонус</th>
              <th className="td" />
            </tr>
            {ownedAgents.map((group, i) => (
                group.agents.map((agent, k) => (
                  <tr key={k} className="tr">
                    <td className="td">
                      <img src="/static/images/default_user.jpg" className="image" alt="Аватар" />
                    </td>
                    <td className="td">{agent.name}</td>
                    <td className="td">{agent.age}</td>
                    <td className="td">{agent.output}</td>
                    <td className="td">{agent.total}</td>
                    <td className="td">
                      {agent.bonusActive ? 'Активен' : 'Не активен'}
                    </td>
                    <td className="td">
                      <div
                        className="timeActionDiv"
                        title="Обучить агента"
                        onClick={() => trainAgent(group.group, agent)}
                      >6
                      </div>
                    </td>
                    <td className="td">
                      <div
                        className="timeActionDiv"
                        title="Мотивировать агента"
                        onClick={() => motivateAgent(group.group, agent)}
                      >3
                      </div>
                    </td>
                  </tr>
                ))
            ))}
          </tbody>
        </table>
        :
        <div>Список пуст. Найдите первого агента и он отобразится в этом окне.</div>
      }
    </div>
  </div>
)

Profile.propTypes = {
  ownedAgents: PropTypes.arrayOf(PropTypes.shape),
  trainAgent: PropTypes.func.isRequired,
  motivateAgent: PropTypes.func.isRequired,
}

export default Profile
