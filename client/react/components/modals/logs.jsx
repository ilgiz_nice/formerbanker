import React, { PropTypes } from 'react'

const Log = ({ logList }) => (
  <div className="modal">
    <h3>Журнал действий</h3>
    <div className="body">
      {logList.map((item, i) => (
        <div key={i} className="logitem">
          <img src="/static/images/default_user.jpg" className="image" alt="Аватар" />
          <div className="logtext">{item.date}: {item.event}</div>
        </div>
      ))}
    </div>
  </div>
)

Log.propTypes = {
  logList: PropTypes.arrayOf(PropTypes.shape),
}

export default Log
