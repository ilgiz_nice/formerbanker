import React, { PropTypes } from 'react'
import { Link } from 'react-router'

const Rating = ({ ratingList }) => (
  <div className="modal">
    <h3>Лучшие игроки</h3>
    <div className="body">
      <table className="table">
        <tbody>
          <tr>
            <th className="td td_first">Игрок</th>
            <th className="td">Кредиты</th>
            <th className="td">Доход в день</th>
            <th className="td td_last" />
          </tr>
          {ratingList.map((player, i) => (
            <tr key={i} className="tr">
              <td className="td td_first">
                <img src="/static/images/default_user.jpg" className="image" alt="Аватар" />
              </td>
              <td className="td">{player.total}</td>
              <td className="td">{player.average}</td>
              <td className="td td_last">
                <Link to={`/users/profile/${player._id}`}>
                  <img src="/static/images/view2.png" className="image" alt="Перейти" />
                </Link>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  </div>
)

Rating.propTypes = {
  ratingList: PropTypes.arrayOf(PropTypes.shape),
}

export default Rating
