import React, { PropTypes } from 'react'

const Dialog = ({ group, agents, company, addAgent }) => (
  <div className="modal toplvl-modal">
    <h3>{company}</h3>
    <h3>Потенциальыне агенты в организации</h3>
    <div className="body">
      <table className="table">
        <tbody>
          <tr>
            <th className="td td_first" />
            <th className="td">Имя</th>
            <th className="td">Возраст</th>
            <th className="td td_last" />
          </tr>
          {agents.map((agent, i) => (
            <tr key={i} className="tr">
              <td className="td td_first">
                <img src="/static/images/default_user.jpg" className="image" alt="Аватар" />
              </td>
              <td className="td">{agent.name}</td>
              <td className="td">{agent.age}</td>
              <td className="td td_last">
                <div
                  className="timeActionDiv"
                  title="Привлечь агента"
                  onClick={() => addAgent(group, agent)}
                >{agent.timeCost}
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  </div>
)

Dialog.propTypes = {
  group: PropTypes.string,
  company: PropTypes.string,
  agents: PropTypes.arrayOf(PropTypes.shape()),
  addAgent: PropTypes.func.isRequired,
}

export default Dialog
