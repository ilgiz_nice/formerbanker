import React, { PropTypes } from 'react'

const Phone = ({ potentialAgents, updateMap }) => (
  <div className="modal">
    <h3>Список личных контактов</h3>
    {potentialAgents.reduce((length, item) => (length + item.agents.length), 0) > 0 ?
      <div className="body">
        <table className="table">
          <tbody>
            <tr>
              <th className="td" />
              <th className="td">Тип контакта</th>
              <th className="td">Стоимость</th>
              <th className="td" />
            </tr>
            {potentialAgents.map((group, i) => (
              <div key={i}>
                <tr className="tr">
                  <td className="td">
                    <img src="/static/images/default_user.jpg" className="image" alt="Аватар" />
                  </td>
                  <td className="td">{group.group}</td>
                  <td className="td">{group.timeCost}</td>
                  <td className="td td_last">
                    <img
                      src="/static/images/target.png"
                      alt="Перейти"
                      className="image"
                      onClick={() => {
                        updateMap(group)
                      }}
                    />
                  </td>
                </tr>
              </div>
            ))}
          </tbody>
        </table>
      </div>
      :
      <div>Потенциальных агентов не найдено.</div>
    }
  </div>
)

Phone.propTypes = {
  potentialAgents: PropTypes.arrayOf(PropTypes.shape),
  updateMap: PropTypes.func.isRequired,
}

export default Phone
