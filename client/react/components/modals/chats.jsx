import React, { PropTypes } from 'react'
// import { Link } from 'react-router'

require('!style!css!sass!../../../../static/styles/modals.scss')

const Chat = ({ tab: { type, target }, chats, switchTab, sendMessage }) => {
  console.log(type, target, chats)
  return (
    <div className="modal">
      <h3>Чат</h3>
      <ul className="tabs">
        <li
          className={type === 'all' && 'active'}
          onClick={() => switchTab({ type: 'all' })}
        >Общий</li>
        {chats.cities.map((city, i) => (
          <div>
            <li
              key={i}
              className={type === 'cities' && target === city.name && 'active'}
              onClick={() => switchTab({ type: 'cities', target: city.name })}
            >{city.name}</li>
          </div>
        ))}
        {chats.personal.map((person, i) => (
          <div>
            <li
              key={i}
              className={type === 'personal' && target === person.name && 'active'}
              onClick={() => switchTab({ type: 'personal', target: person.name })}
            >{person.name}</li>
          </div>
        ))}
      </ul>
      <div className="chat">
        {(() => {
          if (type === 'all') {
            return chats[type].map((message, i) => (
              <div key={i}>
                {`${message.from}: `}
                {message.to ? `${message.to}, ` : ''}
                {message.text}
              </div>
            ))
          }
          return chats[type]
            .filter(el => el.name === target)[0].messages
            .map((message, i) => (
              <div key={i}>
                {`${message.from}: `}
                {message.to ? `${message.to}, ` : ''}
                {message.text}
              </div>
            ))
        })()}
      </div>
      <form action="" className="form">
        <input type="text" id="message" placeholder="Введите сообщение..." />
        <button
          type="submit"
          onClick={(e) => {
            e.preventDefault()
            sendMessage({
              type,
              target,
              from: window.__USER__.name,
              to: 'Пользователь',
              text: document.getElementById('message').value,
            })
          }}
        >Отправить</button>
      </form>
    </div>
  )
}

Chat.propTypes = {
  tab: PropTypes.shape(),
  chats: PropTypes.shape(),
  switchTab: PropTypes.func.isRequired,
  sendMessage: PropTypes.func.isRequired,
}

Chat.defaultProps = {
  tab: 0,
  chats: {},
}

export default Chat
