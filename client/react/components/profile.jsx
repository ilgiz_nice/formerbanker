import React, { PropTypes } from 'react'
import { Link } from 'react-router'

const Profile = ({ id, name, city }) => (
  <div>
    <h1>Профиль игрока</h1>
    <h3>Имя - {name}</h3>
    <h3>Город - {city}</h3>
    <Link to={`/users/${id}`}>
      <button>К игроку</button>
    </Link>
    <Link to={'/'}>
      <button>Вернуться</button>
    </Link>
  </div>
)

Profile.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  city: PropTypes.string,
}

export default Profile
