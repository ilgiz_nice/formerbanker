import { connect } from 'react-redux'

import Profile from '../components/index.jsx'
import { aProfileAsync } from '../../../redux/actions/async.js'

const mapStateToProps = (state) => {
  console.log(state.async.profile)
  return {
    profile: state.async.profile,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    saveProfileChanges: profileName => (
      dispatch(aProfileAsync.updateProfile({ name: profileName }))
    ),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile)
