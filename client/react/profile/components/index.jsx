import React, { PropTypes } from 'react'
import { Link } from 'react-router'

const Profile = ({
  //  PROPS
  profile: {
    name,
    city,
    update: {
      done,
      success,
      message,
    },
  },
  //  ACTIONS
  saveProfileChanges,
}) => (
  <div>
    {done === true && (
      <div className={success === true ? 'message success' : 'message failure'}>
        {(() => {
          setTimeout(() => {
            document.getElementById('message').innerHTML = ''
          }, 3000)
          return (
            <span id="message">{message}</span>
          )
        })()}
      </div>
    )}
    <h1>Профиль игрока</h1>
    <h3>Имя - </h3>
    <input type="text" id="profileName" defaultValue={name} />
    <h3>Город - {city}</h3>
    <button
      onClick={() => saveProfileChanges(document.getElementById('profileName').value)}
    >Сохранить изменения</button>
    <Link to={'/'}>
      <button>Вернуться</button>
    </Link>
  </div>
)

Profile.propTypes = {
  //  PROPS
  name: PropTypes.string,
  city: PropTypes.string,
  //  ACTIONS
  saveProfileChanges: PropTypes.func.isRequired,
}

export default Profile
