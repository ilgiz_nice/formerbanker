import { connect } from 'react-redux'

import Elements from './../components/elements.jsx'
import {
  aProfile,
  aPhone,
  aHints,
  aRating,
  aLog,
  aChat,
  aDialog,
} from './../../redux/actions/elements'
import aTutorial from './../../redux/actions/tutorial'
import hintsDB from './../../configs/hints'

let visibility

const mapStateToProps = (state, own) => {
  visibility = state.elements
  const isRead = own.mode === 'read'
  const showRating = !state.tutorial.tutorial && state.tutorial.isStarted
  const showLog = !state.tutorial.tutorial && state.tutorial.isStarted
  const showTime = !state.tutorial.tutorial && state.tutorial.isStarted
  const showChat = !state.tutorial.tutorial && state.tutorial.isStarted
  let ownedAgents
  let potentialAgents
  if (isRead) {
    ownedAgents = state.async.user.agents.ownedAgents.reduce((total, group) => (
      total + group.agents.length
    ), 0)
    potentialAgents = {
      groups: state.async.user.agents.potentialAgents.length,
      total: state.async.user.agents.potentialAgents.reduce((total, group) => (
        total + group.agents.length
      ), 0),
    }
  } else {
    ownedAgents = state.async.agents.ownedAgents.reduce((total, group) => (
      total + group.agents.length
    ), 0)
    potentialAgents = {
      groups: state.async.agents.potentialAgents.length,
      total: state.async.agents.potentialAgents.reduce((total, group) => (
        total + group.agents.length
      ), 0),
    }
  }
  return {
    home: isRead,
    profile: state.tutorial.profile,
    phone: state.tutorial.phone,
    rating: showRating,
    log: showLog,
    time: showTime,
    chat: showChat,
    dialog: state.elements.dialog,
    ownedAgents,
    potentialAgents,
    tutorial: state.tutorial.tutorial,
    currentTime: state.async.time,
  }
}

const closeAll = (dispatch, el, visible) => {
  dispatch(aProfile.close())
  dispatch(aPhone.close())
  dispatch(aRating.close())
  dispatch(aLog.close())
  dispatch(aChat.close())
  dispatch(aDialog.close())
  if (!visible) dispatch(el.open())
}

const mapDispatchToProps = (dispatch) => {
  return {
    openProfile: (tutorial) => {
      closeAll(dispatch, aProfile, visibility.profile)
      if (tutorial) {
        dispatch(aTutorial.phone.updatePhoneDB())
        dispatch(aHints.push(hintsDB.slice(6, 7)))
      }
    },
    openPhone: (tutorial) => {
      closeAll(dispatch, aPhone, visibility.phone)
      if (tutorial) {
        dispatch(aTutorial.endTutorial())
        dispatch(aHints.update(hintsDB.slice(7)))
      }
    },
    openRating: () => {
      closeAll(dispatch, aRating, visibility.rating)
    },
    openLog: () => {
      closeAll(dispatch, aLog, visibility.log)
    },
    openChat: () => {
      closeAll(dispatch, aChat, visibility.chat)
    },
    closeDialog: () => {
      dispatch(aDialog.close())
    },
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Elements)
