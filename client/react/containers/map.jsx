import { connect } from 'react-redux'

import Map from './../components/map.jsx'
import { aDialog, aMap } from './../../redux/actions/elements'

const mapStateToProps = (state, own) => {
  const isRead = own.mode === 'read'
  let map
  let ownedAgents
  if (isRead) {
    map = state.async.user.elements.map
    ownedAgents = state.async.user.agents.ownedAgents
  } else {
    map = state.elements.map
    ownedAgents = state.async.agents.ownedAgents
  }
  return {
    map,
    ownedAgents,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    openDialog: () => {
      dispatch(aDialog.open())
    },
    changeMap: (map) => {
      dispatch(aMap.change(map))
    },
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Map)
