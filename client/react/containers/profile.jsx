import { connect } from 'react-redux'

import Profile from './../components/profile.jsx'

const mapStateToProps = (state, own) => {
  return {
    id: own.id,
    name: state.async.user.name,
    city: state.async.user.city,
  }
}

export default connect(
  mapStateToProps,
  null
)(Profile)
