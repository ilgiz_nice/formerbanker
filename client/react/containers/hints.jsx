import { connect } from 'react-redux'

import Hints from './../components/hints.jsx'
import { aHints } from './../../redux/actions/elements'
import aTutorial from './../../redux/actions/tutorial'
import hintsDB from './../../configs/hints'

const mapStateToProps = (state, own) => {
  const isRead = own.mode === 'read'
  return {
    hints: state.elements.hints,
    isStarted: state.tutorial.isStarted,
    read: isRead,
  }
}

const mapDispatchToProps = dispatch => (
  {
    start: () => {
      dispatch(aTutorial.startTutorial())
      dispatch(aTutorial.profile.updateProfileDB())
      dispatch(aHints.update(hintsDB.slice(4, 6)))
    },
  }
)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Hints)
