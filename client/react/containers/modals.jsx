import { connect } from 'react-redux'

import Modals from './../components/modals.jsx'
import {
  aPhone,
  aMap,
  aChat,
  aDialog,
} from './../../redux/actions/elements'
import {
  aTimeAsync,
  aAgentTrainAsync,
  aAgentMotivationAsync,
  aAgents,
  aLogAsync,
} from './../../redux/actions/async.js'

const mapStateToProps = (state, own) => {
  let ownedAgents
  let potentialAgents
  let logList
  if (own.mode === 'read') {
    ownedAgents = state.async.user.agents.ownedAgents
    potentialAgents = state.async.user.agents.potentialAgents
    logList = state.async.user.logs
  } else {
    ownedAgents = state.async.agents.ownedAgents
    potentialAgents = state.async.agents.potentialAgents
    logList = state.async.logs
  }
  return {
    ownedAgents,
    potentialAgents,
    ratingList: state.async.rating,
    logList,
    profile: state.elements.profile,
    phone: state.elements.phone,
    rating: state.elements.rating,
    log: state.elements.log,
    chat: state.elements.chat,
    tab: state.elements.tab,
    chats: state.chats,
    agents: state.elements.agents,
    dialog: state.elements.dialog,
  }
}

const mapDispatchToProps = (dispatch, own) => {
  const isRead = own.mode === 'read'
  return {
    updateMap: (group) => {
      dispatch(aPhone.close())
      dispatch(aMap.update(group.location.lat, group.location.lng, group.group, group.company, group.agents))
      dispatch(aTimeAsync.spendTime(group.timeCost))
    },
    trainAgent: (group, agent) => {
      dispatch(aAgentTrainAsync.trainAgent(group, agent))
    },
    motivateAgent: (group, agent) => {
      dispatch(aAgentMotivationAsync.motivateAgent(group, agent))
    },
    switchTab: ({ type, target }) => {
      dispatch(aChat.switchTab({ type, target }))
    },
    sendMessage: ({ type, target, from, to, text }) => {
      own.sendMessage({ type, target, from, to, text })
    },
    closeDialog: () => {
      dispatch(aDialog.close())
    },
    addAgent: (group, agent) => {
      if (isRead === true) return false
      dispatch(aAgents.addAgent(group, agent))
      dispatch(aLogAsync.writeLog(`Привлёк агента ${agent.name} из группы ${group}`))
    },
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Modals)
