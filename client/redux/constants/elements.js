//  Иконка профиля
export const cProfile = {
  OPEN_PROFILE: 'OPEN_PROFILE',
  CLOSE_PROFILE: 'CLOSE_PROFILE',
}

//  Иконка телефона
export const cPhone = {
  OPEN_PHONE: 'OPEN_PHONE',
  CLOSE_PHONE: 'CLOSE_PHONE',
}

//  Карта
export const cMap = {
  CLICK_MAP: 'CLICK_MAP',
  UPDATE_MAP: 'UPDATE_MAP',
  CHANGE_MAP: 'CHANGE_MAP',
}

//  Подсказки
export const cHints = {
  UPDATE_HINTS: 'UPDATE_HINTS',
  PUSH_HINTS: 'PUSH_HINTS',
}

//  Окно с агентами
export const cDialog = {
  OPEN_DIALOG: 'OPEN_DIALOG',
  CLOSE_DIALOG: 'CLOSE_DIALOG',
}

//  Рейтинг
export const cRating = {
  OPEN_RATING: 'OPEN_RATING',
  CLOSE_RATING: 'CLOSE_RATING',
}

//  Логи
export const cLog = {
  OPEN_LOG: 'OPEN_LOG',
  CLOSE_LOG: 'CLOSE_LOG',
}

//  Чат
export const cChat = {
  OPEN_CHAT: 'OPEN_CHAT',
  CLOSE_CHAT: 'CLOSE_CHAT',
  SWITCH_TAB: 'SWITCH_TAB',
}
