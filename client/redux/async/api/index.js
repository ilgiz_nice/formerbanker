import request from 'superagent'

const startTutorial = () => {
  const url = '/api/tutorial/start'
  request.post(url).end((err, res) => {
    if (err) return err
    return res
  })
}

const startGame = () => {
  const url = '/api/tutorial/game'
  request.post(url).end((err, res) => {
    if (err) return err
    return res
  })
}

const updateProfile = () => {
  const url = '/api/tutorial/profile'
  request.post(url).end((err, res) => {
    if (err) return err
    return res
  })
}

const updatePhone = () => {
  const url = '/api/tutorial/phone'
  request.post(url).end((err, res) => {
    if (err) return err
    return res
  })
}

const endTutorial = () => {
  const url = '/api/tutorial/end'
  request.post(url).end((err, res) => {
    if (err) return err
    return res
  })
}

const addAgent = payload => (
  new Promise((resolve, reject) => {
    const url = '/api/agents/add'
    request.post(url).send({ payload }).end((err, res) => {
      if (err) reject(err)
      else resolve(JSON.parse(res.text))
    })
  })
)

const writeLog = payload => (
  new Promise((resolve, reject) => {
    const url = '/api/logs/write'
    request.post(url).send({ payload }).end((err, response) => {
      if (err) reject(err)
      else resolve(JSON.parse(response.text))
    })
  })
)

const fetchUser = payload => (
  new Promise((resolve, reject) => {
    const url = `/api/users/${payload.id}`
    request.get(url).end((err, response) => {
      if (err) reject(err)
      else resolve(JSON.parse(response.text))
    })
  })
)

const spendTime = payload => (
  new Promise((resolve, reject) => {
    const url = '/api/time/spend'
    request.post(url).send({ payload }).end((err, response) => {
      if (err) reject(err)
      else resolve(JSON.parse(response.text))
    })
  })
)

const trainAgent = payload => (
  new Promise((resolve, reject) => {
    const url = '/api/agents/train'
    request.post(url).send({ payload }).end((err, response) => {
      if (err) reject(err)
      else resolve(JSON.parse(response.text))
    })
  })
)

const motivateAgent = payload => (
  new Promise((resolve, reject) => {
    const url = '/api/agents/motivate'
    request.post(url).send({ payload }).end((err, response) => {
      if (err) reject(err)
      else resolve(JSON.parse(response.text))
    })
  })
)

const profile = {
  updateProfile: payload => (
    new Promise((resolve, reject) => {
      const url = '/api/profile/update'
      request.post(url).send({ payload }).end((err, response) => {
        if (err) reject(err)
        else resolve(JSON.parse(response.text))
      })
    })
  ),
}

export default {
  startTutorial,
  startGame,
  updateProfile,
  updatePhone,
  endTutorial,
  addAgent,
  writeLog,
  fetchUser,
  spendTime,
  trainAgent,
  motivateAgent,
  profile,
}
