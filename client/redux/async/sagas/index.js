import { takeEvery, takeLatest } from 'redux-saga'
import { call, put, fork } from 'redux-saga/effects'

import cTutorial from './../../constants/tutorial'
import {
  cAgents,
  cLogAsync,
  cUserAsync,
  cTimeAsync,
  cAgentTrainAsync,
  cAgentMotivationAsync,
  cProfileAsync,
} from './../../constants/async'
import aTutorial from './../../actions/tutorial'
import {
  aAgents,
  aLogAsync,
  aUserAsync,
  aTimeAsync,
  aAgentTrainAsync,
  aAgentMotivationAsync,
  aProfileAsync,
} from './../../actions/async'
import API from './../api/index'

function* startTutorial() {
  try {
    yield call(API.startTutorial)
    yield call(API.startGame)
    yield put(aTutorial.startTutorialSuccess())
  } catch (e) {
    yield put(aTutorial.startTutorialFailure(e.message))
  }
}

function* updateProfile() {
  try {
    yield call(API.updateProfile)
    yield put(aTutorial.profile.updateProfileDBSuccess())
  } catch (e) {
    yield put(aTutorial.profile.updateProfileDBFailure(e.message))
  }
}

function* updatePhone() {
  try {
    yield call(API.updatePhone)
    yield put(aTutorial.phone.updatePhoneDBSuccess())
  } catch (e) {
    yield put(aTutorial.phone.updatePhoneDBFailure(e.message))
  }
}

function* endTutorial() {
  try {
    yield call(API.endTutorial)
    yield put(aTutorial.endTutorialSuccess())
  } catch (e) {
    yield put(aTutorial.endTutorialFailure(e.message))
  }
}

function* addAgent(action) {
  try {
    const agents = yield call(API.addAgent, action.payload)
    yield put(aAgents.addAgentSuccess(agents))
  } catch (e) {
    yield put(aAgents.addAgentFailure(e.message))
  }
}

function* writeLog(action) {
  try {
    const logs = yield call(API.writeLog, action.payload)
    yield put(aLogAsync.writeLogSuccess(logs))
  } catch (e) {
    yield put(aLogAsync.writeLogFailure(e.message))
  }
}

function* fetchUser(action) {
  try {
    const user = yield call(API.fetchUser, action.payload)
    yield put(aUserAsync.fetchUserSuccess(user))
  } catch (e) {
    yield put(aUserAsync.fetchUserFailure(e.message))
  }
}

function* spendTime(action) {
  try {
    const time = yield call(API.spendTime, action.payload)
    yield put(aTimeAsync.spendTimeSuccess(time))
  } catch (e) {
    yield put(aTimeAsync.spendTimeFailure(e.message))
  }
}

function* trainAgent(action) {
  try {
    const agents = yield call(API.trainAgent, action.payload)
    yield put(aAgentTrainAsync.trainAgentSuccess(agents))
  } catch (e) {
    yield put(aAgentTrainAsync.trainAgentFailure(e.message))
  }
}

function* motivateAgent(action) {
  try {
    const agents = yield call(API.motivateAgent, action.payload)
    yield put(aAgentMotivationAsync.motivateAgentSuccess(agents))
  } catch (e) {
    yield put(aAgentMotivationAsync.motivateAgentFailure(e.message))
  }
}

function* profileUpdateProfile(action) {
  try {
    const user = yield call(API.profile.updateProfile, action.payload)
    yield put(aProfileAsync.updateProfileSuccess(user))
  } catch (e) {
    yield put(aProfileAsync.updateProfileFailure(e.message))
  }
}

function* watchStartTutorial() {
  yield* takeEvery(cTutorial.START_TUTORIAL, startTutorial)
}

function* watchUpdateProfile() {
  yield* takeEvery(cTutorial.PROFILE.UPDATE_PROFILE_DB, updateProfile)
}

function* watchUpdatePhone() {
  yield* takeEvery(cTutorial.PHONE.UPDATE_PHONE_DB, updatePhone)
}

function* watchEndTutorial() {
  yield* takeEvery(cTutorial.END_TUTORIAL, endTutorial)
}

function* watchAddAgent() {
  yield* takeEvery(cAgents.ADD_AGENT, addAgent)
}

function* watchWriteLog() {
  yield* takeEvery(cLogAsync.WRITE_LOG, writeLog)
}

function* watchFetchUser() {
  yield* takeEvery(cUserAsync.FETCH_USER, fetchUser)
}

function* watchSpendTime() {
  yield* takeEvery(cTimeAsync.SPEND_TIME, spendTime)
}

function* watchTrainAgent() {
  yield* takeEvery(cAgentTrainAsync.TRAIN_AGENT, trainAgent)
}

function* watchMotivateAgent() {
  yield* takeEvery(cAgentMotivationAsync.MOTIVATE_AGENT, motivateAgent)
}

function* watchProfileUpdateProfile() {
  yield* takeLatest(cProfileAsync.UPDATE_PROFILE, profileUpdateProfile)
}

export default function* async() {
  yield [
    fork(watchStartTutorial),
    fork(watchUpdateProfile),
    fork(watchUpdatePhone),
    fork(watchEndTutorial),
    fork(watchAddAgent),
    fork(watchWriteLog),
    fork(watchFetchUser),
    fork(watchSpendTime),
    fork(watchTrainAgent),
    fork(watchMotivateAgent),
    fork(watchProfileUpdateProfile),
  ]
}
