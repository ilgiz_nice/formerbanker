import {
  cProfile,
  cPhone,
  cMap,
  cHints,
  cDialog,
  cRating,
  cLog,
  cChat,
} from './../constants/elements'

export const aProfile = {
  open: () => ({ type: cProfile.OPEN_PROFILE }),
  close: () => ({ type: cProfile.CLOSE_PROFILE }),
}

export const aPhone = {
  open: () => ({ type: cPhone.OPEN_PHONE }),
  close: () => ({ type: cPhone.CLOSE_PHONE }),
}

export const aMap = {
  click: () => ({ type: cMap.CLICK_MAP }),
  update: (lat, lng, group, company, agents) => ({
    type: cMap.UPDATE_MAP,
    payload: { lat, lng, group, company, agents },
  }),
  change: map => ({ type: cMap.CHANGE_MAP, payload: { map } }),
}

export const aHints = {
  update: hints => ({ type: cHints.UPDATE_HINTS, payload: { hints } }),
  push: hint => ({ type: cHints.PUSH_HINTS, payload: { hint } }),
}

export const aDialog = {
  open: () => ({ type: cDialog.OPEN_DIALOG }),
  close: () => ({ type: cDialog.CLOSE_DIALOG }),
}

export const aRating = {
  open: () => ({ type: cRating.OPEN_RATING }),
  close: () => ({ type: cRating.CLOSE_RATING }),
}

export const aLog = {
  open: () => ({ type: cLog.OPEN_LOG }),
  close: () => ({ type: cLog.CLOSE_LOG }),
}

export const aChat = {
  open: () => ({ type: cChat.OPEN_CHAT }),
  close: () => ({ type: cChat.CLOSE_CHAT }),
  switchTab: ({ type, target }) => ({ type: cChat.SWITCH_TAB, payload: { type, target } }),
}
