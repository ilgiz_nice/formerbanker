import {
  cAgents,
  cRating,
  cLogAsync,
  cUserAsync,
  cTimeAsync,
  cAgentTrainAsync,
  cAgentMotivationAsync,
  cChatAsync,
  cProfileAsync,
} from './../constants/async'

export const aAgents = {
  addAgent: (group, agent) => ({ type: cAgents.ADD_AGENT, payload: { group, agent } }),
  addAgentSuccess: agents => ({ type: cAgents.ADD_AGENT_SUCCESS, payload: { agents } }),
  addAgentFailure: err => ({ type: cAgents.ADD_AGENT_FAILURE, payload: { err } }),
}

export const aRating = {
  getRating: () => ({ type: cRating.GET_RATING }),
  getRatingSuccess: rating => ({ type: cRating.GET_RATING_SUCCESS, payload: { rating } }),
  getRatingFailure: err => ({ type: cRating.GET_RATING_FAILURE, payload: { err } }),
}

export const aLogAsync = {
  writeLog: event => ({ type: cLogAsync.WRITE_LOG, payload: { event } }),
  writeLogSuccess: logs => ({ type: cLogAsync.WRITE_LOG_SUCCESS, payload: { logs } }),
  writeLogFailure: err => ({ type: cLogAsync.WRITE_LOG_FAILURE, payload: { err } }),
}

export const aUserAsync = {
  fetchUser: id => ({ type: cUserAsync.FETCH_USER, payload: { id } }),
  fetchUserSuccess: user => ({ type: cUserAsync.FETCH_USER_SUCCESS, payload: { user } }),
  fetchUserFailure: err => ({ type: cUserAsync.FETCH_USER_FAILURE, payload: { err } }),
}

export const aTimeAsync = {
  spendTime: amount => ({ type: cTimeAsync.SPEND_TIME, payload: { amount } }),
  spendTimeSuccess: time => ({ type: cTimeAsync.SPEND_TIME_SUCCESS, payload: { time } }),
  spendTimeFailure: err => ({ type: cTimeAsync.SPEND_TIME_FAILURE, payload: { err } }),
}

export const aAgentTrainAsync = {
  trainAgent: (group, agent) => ({ type: cAgentTrainAsync.TRAIN_AGENT, payload: { group, agent } }),
  trainAgentSuccess: agents => ({ type: cAgentTrainAsync.TRAIN_AGENT_SUCCESS, payload: { agents } }),
  trainAgentFailure: err => ({ type: cAgentTrainAsync.TRAIN_AGENT_FAILURE, payload: { err } }),
}

export const aAgentMotivationAsync = {
  motivateAgent: (group, agent) => ({ type: cAgentMotivationAsync.MOTIVATE_AGENT, payload: { group, agent } }),
  motivateAgentSuccess: agents => ({ type: cAgentMotivationAsync.MOTIVATE_AGENT_SUCCESS, payload: { agents } }),
  motivateAgentFailure: err => ({ type: cAgentMotivationAsync.MOTIVATE_AGENT_FAILURE, payload: { err } }),
}

export const aChatAsync = {
  sendMessage: ({ type, target, from, to, text }) => ({ type: cChatAsync.SEND_MESSAGE, payload: { type, target, from, to, text } }),
  receiveMessage: ({ type, target, from, to, text }) => ({ type: cChatAsync.RECEIVE_MESSAGE, payload: { type, target, from, to, text } }),
}

export const aProfileAsync = {
  updateProfile: ({ name }) => ({ type: cProfileAsync.UPDATE_PROFILE, payload: { name } }),
  updateProfileSuccess: ({ user }) => ({ type: cProfileAsync.UPDATE_PROFILE_SUCCESS, payload: { user } }),
  updateProfileFailure: ({ err }) => ({ type: cProfileAsync.UPDATE_PROFILE_FAILURE, payload: { err } }),
}
