import cTutorial from './../constants/tutorial'

export default {
  startTutorial: () => ({ type: cTutorial.START_TUTORIAL }),
  startTutorialSuccess: () => ({ type: cTutorial.START_TUTORIAL_SUCCESS }),
  startTutorialFailure: err => ({ type: cTutorial.START_TUTORIAL_FAILURE, payload: { err } }),
  profile: {
    updateProfileDB: () => ({ type: cTutorial.PROFILE.UPDATE_PROFILE_DB }),
    updateProfileDBSuccess: () => ({ type: cTutorial.PROFILE.UPDATE_PROFILE_DB_SUCCESS }),
    updateProfileDBFailure: err => ({
      type: cTutorial.PROFILE.UPDATE_PROFILE_DB_FAILURE,
      payload: { err },
    }),
  },
  phone: {
    updatePhoneDB: () => ({ type: cTutorial.PHONE.UPDATE_PHONE_DB }),
    updatePhoneDBSuccess: () => ({ type: cTutorial.PHONE.UPDATE_PHONE_DB_SUCCESS }),
    updatePhoneDBFailure: err => ({
      type: cTutorial.PHONE.UPDATE_PHONE_DB_FAILURE,
      payload: { err },
    }),
  },
  endTutorial: () => ({ type: cTutorial.END_TUTORIAL }),
  endTutorialSuccess: () => ({ type: cTutorial.END_TUTORIAL_SUCCESS }),
  endTutorialFailure: err => ({ type: cTutorial.END_TUTORIAL_FAILURE, payload: { err } }),
}
