import {
  cAgents,
  cRating,
  cLogAsync,
  cUserAsync,
  cTimeAsync,
  cAgentTrainAsync,
  cAgentMotivationAsync,
  cProfileAsync,
} from './../constants/async'

const rAsyncDefault = {
  profile: {
    name: null,
    city: null,
    update: {
      done: false,
      success: true,
      message: '',
    },
  },
  agents: {
    ownedAgents: [],
    potentialAgents: [],
  },
  rating: [],
  logs: [],
  time: [],
  user: {
    name: null,
    city: null,
    elements: {
      profile: false,
      phone: false,
      dialog: false,
      rating: false,
      log: false,
      map: {
        lat: 55.755420,
        lng: 37.622421,
        zoom: 11,
      },
    },
    agents: {
      ownedAgents: [],
      potentialAgents: [],
    },
    logs: [],
  },
}

export default (state = rAsyncDefault, action) => {
  switch (action.type) {
    //  ADD AGENT
    case cAgents.ADD_AGENT:
      return state
    case cAgents.ADD_AGENT_SUCCESS:
      return Object.assign({}, state, {
        agents: {
          ownedAgents: action.payload.agents.agentsB.ownedAgents,
          potentialAgents: action.payload.agents.agentsB.potentialAgents,
        },
        time: action.payload.agents.userC.time,
      })
    case cAgents.ADD_AGENT_FAILURE:
      console.log(action.payload.err)
      return state
    //  GET RATING
    case cRating.GET_RATING:
      return state
    case cRating.GET_RATING_SUCCESS:
      return { ...state, rating: action.payload.rating }
    case cRating.GET_RATING_FAILURE:
      console.log(action.payload.err)
      return state
    //  WRITE LOG
    case cLogAsync.WRITE_LOG:
      return state
    case cLogAsync.WRITE_LOG_SUCCESS:
      return Object.assign({}, state, {
        logs: [...state.logs, ...action.payload.logs.logs],
      })
    case cLogAsync.WRITE_LOG_FAILURE:
      console.log(action.payload.err)
      return state
    //  FETCH USER
    case cUserAsync.FETCH_USER:
      return state
    case cUserAsync.FETCH_USER_SUCCESS:
      return Object.assign({}, state, {
        user: {
          name: action.payload.user.owner.name,
          city: action.payload.user.owner.city,
          elements: {
            map: {
              lat: action.payload.user.owner.location.lat,
              lng: action.payload.user.owner.location.lng,
              zoom: 11,
            },
          },
          agents: {
            ownedAgents: action.payload.user.agent.ownedAgents,
            potentialAgents: action.payload.user.agent.potentialAgents,
          },
          logs: action.payload.user.log.logs,
        },
      })
    case cUserAsync.FETCH_USER_FAILURE:
      console.log(action.payload.err)
      return state
    //  SPEND TIME
    case cTimeAsync.SPEND_TIME:
      return state
    case cTimeAsync.SPEND_TIME_SUCCESS:
      return { ...state, time: action.payload.time.time }
    case cTimeAsync.SPEND_TIME_FAILURE:
      console.log(action.payload.err)
      return state
    //  TRAIN AGENT
    case cAgentTrainAsync.TRAIN_AGENT:
      return state
    case cAgentTrainAsync.TRAIN_AGENT_SUCCESS:
      console.log(action)
      return Object.assign({}, state, {
        agents: {
          ownedAgents: action.payload.agents.agents.ownedAgents,
          potentialAgents: action.payload.agents.agents.potentialAgents,
        },
        time: action.payload.agents.user.time,
      })
    case cAgentTrainAsync.TRAIN_AGENT_FAILURE:
      return state
    //  MOTIVATE AGENT
    case cAgentMotivationAsync.MOTIVATE_AGENT:
      return state
    case cAgentMotivationAsync.MOTIVATE_AGENT_SUCCESS:
      console.log(action)
      return Object.assign({}, state, {
        agents: {
          ownedAgents: action.payload.agents.agents.ownedAgents,
          potentialAgents: action.payload.agents.agents.potentialAgents,
        },
        time: action.payload.agents.user.time,
      })
    case cAgentMotivationAsync.MOTIVATE_AGENT_FAILURE:
      console.log(action.payload.err)
      return state
    case cProfileAsync.UPDATE_PROFILE:
      return state
    case cProfileAsync.UPDATE_PROFILE_SUCCESS:
      return Object.assign({}, state, {
        profile: {
          name: action.payload.user.name,
          city: state.profile.city,
          update: {
            done: true,
            success: true,
            message: 'Профиль успешно обновлён',
          },
        },
      })
    case cProfileAsync.UPDATE_PROFILE_FAILURE:
      return Object.assign({}, state, {
        profile: {
          name: state.profile.name,
          city: state.profile.city,
          update: {
            done: true,
            success: false,
            message: `Возникла ошибка при обновлении профиля: ${action.payload.err}`,
          },
        },
      })
    default:
      return state
  }
}
