import cTutorial from './../constants/tutorial'

const rTutorialDefault = {
  isStarted: false,
  tutorial: false,
  profile: false,
  phone: false,
}

export default (state = rTutorialDefault, action) => {
  switch (action.type) {
    case cTutorial.START_TUTORIAL:
      return state
    case cTutorial.START_TUTORIAL_SUCCESS:
      return { ...state, isStarted: true, tutorial: true }
    case cTutorial.START_TUTORIAL_FAILURE:
      console.log(action.payload.err)
      return state
    case cTutorial.PROFILE.UPDATE_PROFILE_DB:
      return state
    case cTutorial.PROFILE.UPDATE_PROFILE_DB_SUCCESS:
      return { ...state, profile: true }
    case cTutorial.PROFILE.UPDATE_PROFILE_DB_FAILURE:
      console.log(action.payload.err)
      return state
    case cTutorial.PHONE.UPDATE_PHONE_DB:
      return state
    case cTutorial.PHONE.UPDATE_PHONE_DB_SUCCESS:
      return { ...state, phone: true }
    case cTutorial.PHONE.UPDATE_PHONE_DB_FAILURE:
      console.log(action.payload.err)
      return state
    case cTutorial.END_TUTORIAL:
      return state
    case cTutorial.END_TUTORIAL_SUCCESS:
      return { ...state, tutorial: false }
    case cTutorial.END_TUTORIAL_FAILURE:
      console.log(action.payload.err)
      return state
    default:
      return state
  }
}
