import {
  cProfile,
  cPhone,
  cMap,
  cHints,
  cDialog,
  cRating,
  cLog,
  cChat,
} from './../constants/elements'
import hintsDB from './../../configs/hints'

const rElementsDefault = {
  profile: false,
  phone: false,
  dialog: false,
  rating: false,
  log: false,
  chat: false,
  tab: {
    type: 'all',
    target: null,
  },
  map: {
    lat: 55.755420,
    lng: 37.622421,
    zoom: 11,
    showMarker: false,
  },
  agents: {
    group: null,
    company: null,
    agents: [],
  },
  hints: hintsDB.slice(0, 4),
}

export default (state = rElementsDefault, action) => {
  switch (action.type) {
    case cProfile.OPEN_PROFILE:
      return { ...state, profile: true }
    case cProfile.CLOSE_PROFILE:
      return { ...state, profile: false }
    case cPhone.OPEN_PHONE:
      return { ...state, phone: true }
    case cPhone.CLOSE_PHONE:
      return { ...state, phone: false }
    case cDialog.OPEN_DIALOG:
      return { ...state, dialog: true }
    case cDialog.CLOSE_DIALOG:
      return { ...state, dialog: false }
    case cRating.OPEN_RATING:
      return { ...state, rating: true }
    case cRating.CLOSE_RATING:
      return { ...state, rating: false }
    case cLog.OPEN_LOG:
      return { ...state, log: true }
    case cLog.CLOSE_LOG:
      return { ...state, log: false }
    case cChat.OPEN_CHAT:
      return { ...state, chat: true }
    case cChat.CLOSE_CHAT:
      return { ...state, chat: false }
    case cChat.SWITCH_TAB:
      return { ...state, tab: action.payload }
    case cMap.CLICK_MAP:
      return { ...state, map: true }
    case cMap.UPDATE_MAP:
      return Object.assign({}, state, {
        map: {
          lat: action.payload.lat,
          lng: action.payload.lng,
          zoom: 15,
          showMarker: true,
        },
        agents: {
          group: action.payload.group,
          company: action.payload.company,
          agents: action.payload.agents,
        },
      })
    case cMap.CHANGE_MAP:
      return Object.assign({}, state, {
        map: {
          lat: state.map.lat,
          lng: state.map.lng,
          zoom: action.payload.map.zoom,
          showMarker: state.map.showMarker,
        },
      })
    case cHints.UPDATE_HINTS:
      return { ...state, hints: action.payload.hints }
    case cHints.PUSH_HINTS:
      return Object.assign({}, state, {
        hints: [...state.hints, ...action.payload.hint],
      })
    default:
      return state
  }
}
