import { combineReducers } from 'redux'

import rTutorial from './tutorial'
import rElements from './elements'
import rAsync from './async'
import rChats from './chats'

export default combineReducers({
  tutorial: rTutorial,
  elements: rElements,
  async: rAsync,
  chats: rChats,
})
