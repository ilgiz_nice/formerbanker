import {
  cChatAsync,
} from './../constants/async'

const rChatsDefault = {
  all: [],
  cities: [],
  personal: [],
}

const receiveMessage = (state, payload) => {
  console.log(payload)
  if (payload.type === 'all') {
    return Object.assign({}, state, {
      all: [...state.all, {
        from: payload.from,
        to: payload.to,
        text: payload.text,
      }],
    })
  } else if (payload.type === 'cities') {
    const cities = state.cities.map(city => city.name)
    const index = cities.indexOf(payload.target)
    if (index === -1) {
      return Object.assign({}, state, {
        cities: [...state.cities, {
          name: payload.target,
          messages: [{
            from: payload.from,
            to: payload.to,
            text: payload.text,
          }],
        }],
      })
    }
    const citiesUpdated = state.cities.map((city, i) => {
      if (i === index) {
        city.messages.push({
          from: payload.from,
          to: payload.to,
          text: payload.text,
        })
      }
      return city
    })
    return Object.assign({}, state, {
      cities: citiesUpdated,
    })
  } else if (payload.type === 'personal') {
    const personal = state.personal.map(person => person.name)
    const index = personal.indexOf(payload.target)
    if (index === -1) {
      return Object.assign({}, state, {
        personal: [...state.personal, {
          name: payload.target,
          messages: [{
            from: payload.from,
            to: payload.to,
            text: payload.text,
          }],
        }],
      })
    }
    const personalUpdated = state.personal.map((person, i) => {
      if (i === index) {
        person.messages.push({
          from: payload.from,
          to: payload.to,
          text: payload.text,
        })
      }
      return person
    })
    return Object.assign({}, state, {
      personal: personalUpdated,
    })
  }

  return state
}

export default (state = rChatsDefault, action) => {
  switch (action.type) {
    case cChatAsync.SEND_MESSAGE:
      return state
    case cChatAsync.RECEIVE_MESSAGE:
      return receiveMessage(state, action.payload)
    default:
      return state
  }
}
