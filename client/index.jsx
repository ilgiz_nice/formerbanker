import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { Router, Route, browserHistory } from 'react-router'

import Main from './react/pages/index.jsx'
import Profile from './react/profile/template/index.jsx'
import UserProfile from './react/pages/profile.jsx'
import Users from './react/pages/users.jsx'
import reducers from './redux/reducers/'
import async from './redux/async/sagas'
import sockets from './sockets.js'

const preloadedState = window.__PRELOADED_STATE__

const sagaMiddleware = createSagaMiddleware()

const store = createStore(
  reducers,
  preloadedState,
  applyMiddleware(sagaMiddleware)
)

sagaMiddleware.run(async)
const sendMessage = sockets(store)

render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path="/" component={Main} sendMessage={sendMessage} />
      <Route path="/profile" component={Profile} />
      <Route path="/users/profile/:id" component={UserProfile} dispatch={store.dispatch} />
      <Route path="/users/:id" component={Users} dispatch={store.dispatch} />
    </Router>
  </Provider>,
  document.getElementById('app')
)
